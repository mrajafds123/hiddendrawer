import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'screen.dart';
import 'drawerController.dart';

class MenuPage extends DrawerContent {
  MenuPage({Key key, this.title});
  final String title;
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<MenuPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
          icon: new Icon(Icons.dehaze),
          onPressed: () => {
            widget.onMenuPressed()
          },
        ),
        title: Text('Belajar Hidden Drawer'),
      ),
      body: Home(),
    );
  }
}

class MainWidget extends StatefulWidget {
  MainWidget({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MainWidgetState createState() => _MainWidgetState();
}

class _MainWidgetState extends State<MainWidget> with TickerProviderStateMixin {
  HiddenDrawerController _drawerController;
  @override
  void initState() {
    super.initState();
    _drawerController = HiddenDrawerController(
        initialPage: MenuPage(
          title: 'main',
        ),
        items: [
          DrawerItem(
            text: Text(
              'Home',
              style: TextStyle(color: Colors.white),
            ),
            icon: Icon(Icons.home, color: Colors.white),
            page: MenuPage(
              title: 'Home',
            ),
          ),
          DrawerItem(
            text: Text(
              'Gallery',
              style: TextStyle(color: Colors.white),
            ),
            icon: Icon(Icons.photo_album, color: Colors.white),
            page: MenuPage(
              title: 'Gallery',
            ),
          ),
          DrawerItem(
            text: Text(
              'Favorites',
              style: TextStyle(color: Colors.white),
            ),
            icon: Icon(Icons.favorite, color: Colors.white),
            page: MenuPage(
              title: 'Favorites',
            ),
          ),
          DrawerItem(
            text: Text(
              'Settings',
              style: TextStyle(color: Colors.white),
            ),
            icon: Icon(Icons.settings, color: Colors.white),
            page: MenuPage(
              title: 'Settings',
            ),
          )
        ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: HiddenDrawer(
        controller: _drawerController,
        header: Align(
          alignment: Alignment.topLeft,
          child: Column(children: <Widget>[
            Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: Colors.black.withOpacity(0.1), width: 1),
              ),
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              width: MediaQuery.of(context).size.width * 0.4,
              child: ClipOval(
                child: Image(
                  fit: BoxFit.fill,
                  Image.asset('assets/images/JEFF.jpg'),
                ),
              ),
            ),
            SizedBox(
              height: 6,
            ),
            Text(
              'Hallo Meong',
              style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, left: 0),
              child: Text(
                'belajarflutter.com',
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
            )
          ]),
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(begin: Alignment.topLeft, end: Alignment.bottomRight, colors: [
            Colors.deepPurpleAccent.withOpacity(0.5),
            Colors.cyan,
            Colors.teal
          ]),
        ),
      ),
    );
  }
}
